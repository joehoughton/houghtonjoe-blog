﻿namespace houghtonjoe_blog.Web.Tests
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using houghtonjoe_blog.Domain.Error;
    using houghtonjoe_blog.Domain.Infrastructure;
    using houghtonjoe_blog.Domain.User.Models;
    using houghtonjoe_blog.Services.Abstract;
    using houghtonjoe_blog.Services.Utilities;
    using houghtonjoe_blog.Web.Controllers;
    using houghtonjoe_blog.Web.Models;
    using Moq;
    using MyTested.WebApi;
    using MyTested.WebApi.Exceptions;
    using NUnit.Framework;
    using System.Web.Http.Hosting;

    [TestFixture]
    class AccountControllerTests
    {
        /// <summary>
        /// Using the AccountController without injecting dependencies should throw an exception.
        /// </summary>
        [Test]
        [ExpectedException(
        typeof(UnresolvedDependenciesException),
        ExpectedMessage = "AccountController could not be instantiated because it contains no constructor taking no parameters.")]
        public void AccountControllerWithoutDependenciesShouldThrowProperException()
        {
            HttpRequestMessage request = null;
            var user = new LoginViewModel() { Password = "password", Username = "joehoughton" };
            MyWebApi
                .Controller<AccountController>() // no dependencies injected
                .Calling(c => c.Login(request, user))
                .ShouldReturn()
                .Ok();
        }

        /// <summary>
        /// Using the Account controller with the correct dependencies should populate correct action result.
        /// </summary>
        [Test]
        public void AccountControllerWithDependenciesShouldPopulateCorrectActionResult()
        {
            // setup request
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post
            };
            request.Headers.Add("Origin","http://localhost:1487/");
            request.Headers.Add("Referer","http://localhost:1487/");
            request.Headers.Add("Connection", "keep-alive");
            request.Headers.Add("Accept", "application/json");
            request.Content = new StringContent("{'username':'joehoughton','password':'password'}"); // optional
            request.RequestUri = new Uri("http://localhost:1487/api/account/authenticate");

            // setup empty response message for Request.CreateResponse 
            request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            // user - valid credentials don't matter here. Only used to pass to controller
            var user = new LoginViewModel() { Password = "password", Username = "joehoughton" };

            // mock interfaces 
            Mock<IMembershipService> membershipService = new Mock<IMembershipService>();
            Mock<IErrorRepository> errorsRepository = new Mock<IErrorRepository>();
            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();

            // the user to be returned by membership service if login successful
            var userToReturnOnLoginSuccess = new User
            {
                DateCreated = DateTime.Now,
                Email = "joe@email.com",
                HashedPassword = "s34jh34jh3jh4",
                Id = 1,
                IsLocked = false,
                Username = "joehoughton",
                Salt = "salt"
            };

            // mock the ValidateUser method to return a MemberShipContext instance with the userToReturnOnLoginSuccess User
            membershipService.Setup(r => r.ValidateUser(user.Username, user.Password)).Returns(new MembershipContext() { Principal = null, User = userToReturnOnLoginSuccess });

            // build the action result, passing the mocked interfaces to the AccountController and calling the Login method
            var actionResultTestBuilder = MyWebApi
                .Controller<AccountController>()
                .WithResolvedDependencyFor<IMembershipService>(membershipService.Object)
                .WithResolvedDependencyFor<IErrorRepository>(errorsRepository.Object)
                .WithResolvedDependencyFor<IUnitOfWork>(unitOfWork.Object)
                .Calling(c => c.Login(request, user));

            // call the action result
            var actionResult = actionResultTestBuilder.AndProvideTheActionResult();

            Assert.IsNotNull(actionResult);
            Assert.AreEqual(HttpStatusCode.OK, actionResult.StatusCode);
        }

        /// <summary>
        /// Logging in with invalid login credentials should return unsuccessful.
        /// </summary>
        [Test]
        public void CallingWithInvalidLoginCredentialsShouldReturnUnsuccessful()
        {
            // setup request
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post
            };
            request.Headers.Add("Origin", "http://localhost:1487/");
            request.Headers.Add("Referer", "http://localhost:1487/");
            request.Headers.Add("Connection", "keep-alive");
            request.Headers.Add("Accept", "application/json");
            request.Content = new StringContent("{'username':'joehoughton','password':'IncorrectPassword'}"); // optional
            request.RequestUri = new Uri("http://localhost:1487/api/account/authenticate");

            // setup empty response message for Request.CreateResponse 
            request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            // user - valid credentials don't matter here. Only used to pass to controller
            var user = new LoginViewModel() { Password = "IncorrectPassword", Username = "joehoughton" };

            // mock interfaces 
            Mock<IMembershipService> membershipService = new Mock<IMembershipService>();
            Mock<IErrorRepository> errorsRepository = new Mock<IErrorRepository>();
            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();

            // mock the ValidateUser method to return a MemberShipContext instance with a null User i.e. failed login
            membershipService.Setup(r => r.ValidateUser(user.Username, user.Password)).Returns(new MembershipContext() { Principal = null, User = null });

            // build the action result, passing the mocked interfaces to the AccountController and calling the Login method
            var actionResultTestBuilder = MyWebApi
                .Controller<AccountController>()
                .WithResolvedDependencyFor<IMembershipService>(membershipService.Object)
                .WithResolvedDependencyFor<IErrorRepository>(errorsRepository.Object)
                .WithResolvedDependencyFor<IUnitOfWork>(unitOfWork.Object)
                .Calling(c => c.Login(request, user));

            // call the action result
            var actionResult = actionResultTestBuilder.AndProvideTheActionResult();

            // get success status from action result
            var responseContent = ((ObjectContent)actionResult.Content).Value;
            var propertyInfo = responseContent.GetType().GetProperty("success");
            var successvalue = propertyInfo.GetValue(responseContent, null);

            // expect status to be false because incorrect credentials given
            Assert.AreEqual(false, successvalue);
        }

        /// <summary>
        /// Logging in with valid login credentials should return successful.
        /// </summary>
        [Test]
        public void CallingWithValidLoginCredentialsShouldReturnSuccessful()
        {
            // setup request
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post
            };
            request.Headers.Add("Origin", "http://localhost:1487/");
            request.Headers.Add("Referer", "http://localhost:1487/");
            request.Headers.Add("Connection", "keep-alive");
            request.Headers.Add("Accept", "application/json");
            request.Content = new StringContent("{'username':'joehoughton','password':'password'}"); // optional
            request.RequestUri = new Uri("http://localhost:1487/api/account/authenticate");

            // setup empty response message for Request.CreateResponse 
            request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            // user - valid credentials don't matter here. Only used to pass to controller
            var user = new LoginViewModel() { Password = "password", Username = "joehoughton" };

            // mock interfaces 
            Mock<IMembershipService> membershipService = new Mock<IMembershipService>();
            Mock<IErrorRepository> errorsRepository = new Mock<IErrorRepository>();
            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();

            // the user to be returned by membership service if login successful
            var userToReturnOnLoginSuccess = new User
            {
                DateCreated = DateTime.Now,
                Email = "joe@email.com",
                HashedPassword = "s34jh34jh3jh4",
                Id = 1,
                IsLocked = false,
                Username = "joehoughton",
                Salt = "salt"
            };

            // mock the ValidateUser method to return a MemberShipContext instance with the userToReturnOnLoginSuccess User - i.e. login successful (returning null would be unsuccessful)
            membershipService.Setup(r => r.ValidateUser(user.Username, user.Password)).Returns(new MembershipContext() { Principal = null, User = userToReturnOnLoginSuccess });

            // build the action result, passing the mocked interfaces to the AccountController and calling the Login method
            var actionResultTestBuilder = MyWebApi
                .Controller<AccountController>()
                .WithResolvedDependencyFor<IMembershipService>(membershipService.Object)
                .WithResolvedDependencyFor<IErrorRepository>(errorsRepository.Object)
                .WithResolvedDependencyFor<IUnitOfWork>(unitOfWork.Object)
                .Calling(c => c.Login(request, user));

            // call the action result
            var actionResult = actionResultTestBuilder.AndProvideTheActionResult();

            // get success status from action result
            var responseContent = ((ObjectContent)actionResult.Content).Value;
            var propertyInfo = responseContent.GetType().GetProperty("success");
            var successvalue = propertyInfo.GetValue(responseContent, null);

            // expect status to be true because valid credentials given
            Assert.AreEqual(true, successvalue);
        }

        /// <summary>
        /// Registering the with a valid model should return successful.
        /// </summary>
        [Test]
        public void RegisteringWithValidModelShouldReturnSuccessful()
        {
            // setup request
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post
            };
            request.Headers.Add("Origin", "http://localhost:1487/");
            request.Headers.Add("Referer", "http://localhost:1487/");
            request.Headers.Add("Connection", "keep-alive");
            request.Headers.Add("Accept", "application/json");
            request.RequestUri = new Uri("http://localhost:1487/api/account/register");

            // setup empty response message for Request.CreateResponse 
            request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            // mock interfaces 
            Mock<IMembershipService> membershipService = new Mock<IMembershipService>();
            Mock<IErrorRepository> errorsRepository = new Mock<IErrorRepository>();
            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();

            // user - valid credentials
            var user = new RegistrationViewModel() { Password = "password", Username = "joehoughton", Email = "email@email.com" };

            // the user to be returned by membership service
            var userToReturnOnModelSuccess = new User
            {
                DateCreated = DateTime.Now,
                Email = "joe@email.com",
                HashedPassword = "s34jh34jh3jh4",
                Id = 1,
                IsLocked = false,
                Username = "joehoughton",
                Salt = "salt"
            };

            // mock the ValidateUser method to return a MemberShipContext instance with a null User i.e. failed login
            membershipService.Setup(r => r.CreateUser(user.Username, user.Email, user.Password,new []{1})).Returns(userToReturnOnModelSuccess);

            // build the action result, passing the mocked interfaces to the AccountController and calling the Register method
            var actionResultTestBuilder = MyWebApi
                .Controller<AccountController>()
                .WithResolvedDependencyFor<IMembershipService>(membershipService.Object)
                .WithResolvedDependencyFor<IErrorRepository>(errorsRepository.Object)
                .WithResolvedDependencyFor<IUnitOfWork>(unitOfWork.Object)
                .Calling(c => c.Register(request, user));

            // call the action result
            var actionResult = actionResultTestBuilder.AndProvideTheActionResult();

            // get success status from action result
            var responseContent = ((ObjectContent)actionResult.Content).Value;
            var propertyInfo = responseContent.GetType().GetProperty("success");
            var successvalue = propertyInfo.GetValue(responseContent, null);

            // expect status to be true because correct model provided was valid
            Assert.AreEqual(true, successvalue);
        }

        /// <summary>
        /// Registering with an invalid model should return unsuccessful.
        /// </summary>
        [Test]
        public void RegisteringWithInvalidModelShouldReturnUnsuccessful()
        {
            // setup request
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post
            };
            request.Headers.Add("Origin", "http://localhost:1487/");
            request.Headers.Add("Referer", "http://localhost:1487/");
            request.Headers.Add("Connection", "keep-alive");
            request.Headers.Add("Accept", "application/json");
            request.RequestUri = new Uri("http://localhost:1487/api/account/register");

            // setup empty response message for Request.CreateResponse 
            request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            // mock interfaces 
            Mock<IMembershipService> membershipService = new Mock<IMembershipService>();
            Mock<IErrorRepository> errorsRepository = new Mock<IErrorRepository>();
            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();

            // the user to be returned by membership service
            var userToReturnOnModelSuccess = new User
            {
                DateCreated = DateTime.Now,
                Email = "joe@email.com",
                HashedPassword = "s34jh34jh3jh4",
                Id = 1,
                IsLocked = false,
                Username = "joehoughton",
                Salt = "salt"
            };

            // user - pass invalid credentials
            var user = new RegistrationViewModel() { Password = null, Username = null, Email = null };

            // mock the ValidateUser method to return a MemberShipContext instance with a null User i.e. failed login
            membershipService.Setup(r => r.CreateUser(user.Username, user.Email, user.Password, new[] { 1 })).Returns(userToReturnOnModelSuccess);

            // build the action result, passing the mocked interfaces to the AccountController and calling the Login method
            var actionResultTestBuilder = MyWebApi
                .Controller<AccountController>()
                .WithResolvedDependencyFor<IMembershipService>(membershipService.Object)
                .WithResolvedDependencyFor<IErrorRepository>(errorsRepository.Object)
                .WithResolvedDependencyFor<IUnitOfWork>(unitOfWork.Object)
                .Calling(c => c.Register(request, user));

            // call the action result
            var actionResult = actionResultTestBuilder.AndProvideTheActionResult();

            // get success status from action result
            var responseContent = ((ObjectContent)actionResult.Content).Value;
            var propertyInfo = responseContent.GetType().GetProperty("success");
            var successvalue = propertyInfo.GetValue(responseContent, null);

            // expect status to be true because correct model state provided
            Assert.AreEqual(false, successvalue);
        }
    }
}
