﻿namespace houghtonjoe_blog.Web.Tests
{
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using houghtonjoe_blog.Domain.Category;
using houghtonjoe_blog.Domain.Error;
using houghtonjoe_blog.Domain.Infrastructure;
using houghtonjoe_blog.Domain.Post;
using houghtonjoe_blog.Domain.Post.Dto;
using houghtonjoe_blog.Domain.Tag.Dtos;
using houghtonjoe_blog.Web.Controllers;
using Moq;
using MyTested.WebApi;
using NUnit.Framework;

[TestFixture]
class PostsControllerTests
{
    /// <summary>
    /// PaginationSet should return correct number of total pages and results.
    /// </summary>
    [Test]
    public void PaginationSetShouldReturnCorrectNumberOfTotalPagesAndResults()
    {
        // setup request
        var request = new HttpRequestMessage
        {
            Method = HttpMethod.Post
        };

        // setup empty response message for Request.CreateResponse 
        request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

        // mock interfaces 
        Mock<IPostRepository> postRepository = new Mock<IPostRepository>();
        Mock<ICategoryRepository> categoryRepository = new Mock<ICategoryRepository>();
        Mock<IErrorRepository> errorsRepository = new Mock<IErrorRepository>();
        Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();

        // setup pagination variables
        var currentPage = 1;
        var currentPageSize = 6;

        // setup postDtoLists to be returned by GetAll repository method
        var postDtoList = new List<PostDto>()
        {
            new PostDto(){ Id = 1, Title = "Post Title", Description = "Post Description", DateCreated = DateTime.Now, SourceCode = "Source Code", Tags = new List<TagDto>(){}, CategoryId = 1},
            new PostDto(){ Id = 2, Title = "Post Title", Description = "Post Description", DateCreated = DateTime.Now, SourceCode = "Source Code", Tags = new List<TagDto>(){}, CategoryId = 1},
            new PostDto(){ Id = 3, Title = "Post Title", Description = "Post Description", DateCreated = DateTime.Now, SourceCode = "Source Code", Tags = new List<TagDto>(){}, CategoryId = 1},
            new PostDto(){ Id = 4, Title = "Post Title", Description = "Post Description", DateCreated = DateTime.Now, SourceCode = "Source Code", Tags = new List<TagDto>(){}, CategoryId = 1},
            new PostDto(){ Id = 5, Title = "Post Title", Description = "Post Description", DateCreated = DateTime.Now, SourceCode = "Source Code", Tags = new List<TagDto>(){}, CategoryId = 1},
            new PostDto(){ Id = 6, Title = "Post Title", Description = "Post Description", DateCreated = DateTime.Now, SourceCode = "Source Code", Tags = new List<TagDto>(){}, CategoryId = 1}
        };

        // mock the Search repository method to return a list of postDtoList
        postRepository.Setup(r => r.GetAll(currentPage, currentPageSize)).Returns(postDtoList);
        postRepository.Setup(r => r.Count()).Returns(37);

        // build the action result, passing the mocked interfaces to the PostsController and calling the Get method
        var actionResultTestBuilder = MyWebApi
            .Controller<PostsController>()
            .WithResolvedDependencyFor<IPostRepository>(postRepository.Object)
            .WithResolvedDependencyFor<ICategoryRepository>(categoryRepository.Object)
            .WithResolvedDependencyFor<IErrorRepository>(errorsRepository.Object)
            .WithResolvedDependencyFor<IUnitOfWork>(unitOfWork.Object)
            .Calling(c => c.Get(request, currentPage, currentPageSize, null));

        // call the action result
        var actionResult = actionResultTestBuilder.AndProvideTheActionResult();

        // get success status from action result
        var responseContent = ((ObjectContent)actionResult.Content).Value;

        // get total number of pages
        var totalPages = responseContent.GetType().GetProperty("TotalPages");
        var totalPageCount = totalPages.GetValue(responseContent, null);

        // get number of results
        var results = responseContent.GetType().GetProperty("Count");
        var resultCount = results.GetValue(responseContent, null);

        // assert number of pages
        Assert.AreEqual(7, totalPageCount);

        // assert correct number of postDtos are returned
        Assert.AreEqual(6, resultCount);
        }
    }
}
