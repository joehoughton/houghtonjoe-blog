﻿SET IDENTITY_INSERT [dbo].[Category] ON 
INSERT [dbo].[Category] ([Id], [Name], [DateCreated]) VALUES (3, N'Programming', CAST(N'2016-03-03 07:56:29.593' AS DateTime))
INSERT [dbo].[Category] ([Id], [Name], [DateCreated]) VALUES (4, N'General', CAST(N'2016-02-03 09:16:29.593' AS DateTime))
SET IDENTITY_INSERT [dbo].[Category] OFF

SET IDENTITY_INSERT [dbo].[Tag] ON 
INSERT [dbo].[Tag] ([Id], [Title], [DateCreated]) VALUES (1, N'.NET', CAST(N'2015-12-04 07:54:50.227' AS DateTime))
INSERT [dbo].[Tag] ([Id], [Title], [DateCreated]) VALUES (2, N'Angular', CAST(N'2015-12-04 07:54:50.227' AS DateTime))
INSERT [dbo].[Tag] ([Id], [Title], [DateCreated]) VALUES (3, N'Backbone', CAST(N'2015-12-04 07:54:50.227' AS DateTime))
INSERT [dbo].[Tag] ([Id], [Title], [DateCreated]) VALUES (4, N'Node', CAST(N'2015-12-04 07:54:50.227' AS DateTime))
INSERT [dbo].[Tag] ([Id], [Title], [DateCreated]) VALUES (5, N'PHP', CAST(N'2015-12-04 07:54:50.227' AS DateTime))
INSERT [dbo].[Tag] ([Id], [Title], [DateCreated]) VALUES (11, N'Javascript', CAST(N'2015-12-04 07:54:50.227' AS DateTime))
SET IDENTITY_INSERT [dbo].[Tag] OFF