module.exports = function(grunt) {
    grunt.initConfig({ // setup tasks and options for tasks
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            options: {
                sourcemap: 'none', // disable source mapping comments for css and scss files
                style: 'compressed'
            },
            dist: {
                options: {
                    style: 'compressed'
                },
                files: { // set directory for compiled css
                    'houghtonjoe-blog.Web/content/css/site.css': 'houghtonjoe-blog.Web/content/css/site.scss'
                }
            },
            dev: {
                options: {
                    style: 'expanded',
                    sourcemap: 'none'
                },
                files: {
                    'houghtonjoe-blog.Web/content/css/site.css': 'houghtonjoe-blog.Web/content/css/site.scss'
                }
            }
        },
        watch: { // can run individual tasks from command line e.g. grunt watch, grunt sass
            css: {
                files: '**/*.scss', // watch all .scss files
                tasks: ['sass']
            },
            scripts: {
                files: ['houghtonjoe-blog.Web/Scripts/spa/**/*.js'],
                tasks: ['jshint:dev', 'jscs:dev'],
                options: {
                    spawn: false // speeds up the reaction time of the watch
                }
            },
        },
        jshint: {
            dist: {
                options: {
                    jshintrc: '.jshintrc',
                    reporter: require('jshint-stylish'),
                    force: false,
                    ignores: ['houghtonjoe-blog.Web/Scripts/vendors/**']
                },
                src: ['houghtonjoe-blog.Web/Scripts/spa/**/*.js'],
            },
            dev: {
                options: {
                    jshintrc: '.jshintrc',
                    reporter: require('jshint-stylish'),
                    force: true,
                    ignores: ['houghtonjoe-blog.Web/Scripts/vendors/**']
                },
                src: ['houghtonjoe-blog.Web/Scripts/spa/**/*.js'],
            }
        },
        jscs: {
            dist: {
                src: 'houghtonjoe-blog.Web/Scripts/spa/**/*.js',
                options: {
                    config: '.jscsrc',
                    verbose: true,
                    force: false,
                    reporter: require('jscs-stylish').path
                }
            },
            dev: {
                src: 'houghtonjoe-blog.Web/Scripts/spa/**/*.js',
                options: {
                    config: '.jscsrc',
                    verbose: true,
                    force: true,
                    reporter: require('jscs-stylish').path
                }
            }
        },
        minifyHtml: {
            options: {
                cdata: true
            },
            dist: {
                files: {
                    'houghtonjoe-blog.Web/Views/Home/Index.Min.cshtml': 'houghtonjoe-blog.Web/Views/Home/Index.cshtml'
                }
            }
        }

        });


    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-jscs');
	grunt.loadNpmTasks('grunt-minify-html');
	
    grunt.registerTask('build', ['sass:dist', 'jshint:dist', 'jscs:dist', 'minifyHtml']); // run grunt build from command line - production
    grunt.registerTask('default', ['sass:dev', 'watch', 'jshint:dev', 'jscs:dev']); // run grunt from command line - development
};