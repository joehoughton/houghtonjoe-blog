﻿(function(app) {
    'use strict';

    app.directive('footerBar', footerBar);

    function footerBar() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/scripts/spa/layout/footerBar.html'
        }
    }

})(angular.module('common.ui'));