﻿(function () {
    'use strict';

    angular.module('HoughtonJoeBlog', ['common.core', 'common.ui'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
      $routeProvider
        .when("/", {
          templateUrl: "scripts/spa/posts/posts.html",
          controller: "postsCtrl"
        })
        .when("/login", {
          templateUrl: "scripts/spa/account/login.html",
          controller: "loginCtrl"
        })
        .when("/register", {
          templateUrl: "scripts/spa/account/register.html",
          controller: "registerCtrl"
        })
        .when("/posts", {
          templateUrl: "scripts/spa/posts/posts.html",
          controller: "postsCtrl"
        })
        .when("/posts/add", {
          templateUrl: "scripts/spa/posts/add.html",
          controller: "postAddCtrl",
          resolve: { isAuthenticated: isAuthenticated }
        })
        .when("/posts/category/:categoryId", {
          templateUrl: "scripts/spa/posts/posts.html",
          controller: "postsCtrl"
        })
        .when("/posts/tags/:tagId", {
          templateUrl: "scripts/spa/posts/posts.html",
          controller: "postsCtrl"
        })
        .when("/posts/:selectedPostId", {
          templateUrl: "scripts/spa/posts/detail.html",
          controller: "postDetailCtrl"
        })
       .when("/posts/edit/:selectedPostId", {
        templateUrl: "scripts/spa/posts/edit.html",
        controller: "postEditCtrl",
        resolve: { isAuthenticated: isAuthenticated }
      });
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];

    function run($rootScope, $location, $cookieStore, $http) {
        // handle page refreshes
        $rootScope.repository = $cookieStore.get('repository') || {};
        if ($rootScope.repository.loggedUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.repository.loggedUser.authdata;
        }

        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });

            $('.fancybox-media').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                }
            });

            $('[data-toggle=offcanvas]').click(function () {
                $('.row-offcanvas').toggleClass('active');
            });
        });
    }

    isAuthenticated.$inject = ['membershipService', '$rootScope', '$location'];

    function isAuthenticated(membershipService, $rootScope, $location) {
        if (!membershipService.isUserLoggedIn()) {
            $rootScope.previousState = $location.path();
            $location.path('/login');
        }
    }

})();