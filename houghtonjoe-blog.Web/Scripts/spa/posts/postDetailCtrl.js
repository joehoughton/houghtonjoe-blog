﻿(function (app) {
  'use strict';

  app.controller('postDetailCtrl', postDetailCtrl);

  postDetailCtrl.$inject = ['$scope', '$location', '$routeParams', 'apiService', 'notificationService'];

  function postDetailCtrl($scope, $location, $routeParams, apiService, notificationService) {
    $scope.post = {};
    $scope.loadingPost = true;
    $scope.isReadOnly = false;
    $scope.postId = $routeParams.selectedPostId;
    $scope.deletePost = deletePost;
    $scope.navigateToHome = navigateToHome;

    function loadpost() {
      $scope.loadingPost = true;
      apiService.get('/api/posts/detail/' + $scope.postId, null,
      postLoadCompleted,
      postLoadFailed);
    }

    function postLoadCompleted(result) {
      $scope.post = result.data;
      $scope.loadingPost = false;
      setTimeout(highlightSourceCode, 1000); // highlight code after angular bindings complete
    }

    function highlightSourceCode() {
      var element = document.getElementById('sourceCode');
      SyntaxHighlighter.highlight(undefined, element);
    }

    function postLoadFailed() {
      notificationService.displayError("Failed to load post");
    }

    function deletePost() {
      apiService.del('/api/posts/' + $scope.postId, null,
      deletePostSucceded,
      deletePostFailed);
    }

    function deletePostSucceded() {
      notificationService.displaySuccess('Deleted post');
      $scope.navigateToHome();
    }

    function deletePostFailed() {
      notificationService.displayError("Failed to delete post");
    }

    function navigateToHome() {
      $location.url('/');
    }

    loadpost();
  }

})(angular.module('HoughtonJoeBlog'));
