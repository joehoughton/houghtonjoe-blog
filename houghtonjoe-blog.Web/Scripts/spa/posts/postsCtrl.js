﻿(function (app) {
  'use strict';

  app.controller('postsCtrl', postsCtrl);

  postsCtrl.$inject = ['$scope', '$routeParams', '$timeout', 'apiService', 'notificationService'];

  function postsCtrl($scope, $routeParams, $timeout, apiService, notificationService) {
    $scope.pageClass = 'page-posts';
    $scope.loadingPosts = true;
    $scope.loadingCategories = true;
    $scope.loadingTags = true;
    $scope.page = 0;
    $scope.pagesCount = 0;
    $scope.categoryId = $routeParams.categoryId;
    $scope.tagId = $routeParams.tagId;
    $scope.Posts = [];
    $scope.Categories = [];
    $scope.Tags = [];
    $scope.search = search;
    $scope.loadCategories = loadCategories;
    $scope.loadTags = loadTags;

    $('li.dropdown').find('.fa-angle-down').each(function () { // responsive navigation menu
      $(this).on('click', function () {
        if ($(window).width() < 768) {
          $(this).parent().next().slideToggle();
        }
        return false;
      });
    });

    $("body").niceScroll({ styler: "fb", cursorcolor: "#000" }); // smooth scroll

    function search(page) {
      page = page || 0;

      $scope.loadingPosts = true;

      var config = {
        params: {
          page: page,
          pageSize: 3,
          filter: $scope.filterPosts
        }
      };

      if ($scope.tagId) { // search by tagId
        config.params.tagId = $scope.tagId;
        apiService.get('/api/posts/tag', config,
          loadPostsCompleted,
          loadPostsFailed);
      } else if ($scope.categoryId) { // search by categoryId
        config.params.categoryId = $scope.categoryId;
        apiService.get('/api/posts/category', config,
          loadPostsCompleted,
          loadPostsFailed);
      } else { // search for all
        apiService.get('/api/posts/', config,
          loadPostsCompleted,
          loadPostsFailed);
      }
    }

    function loadPostsCompleted(result) {
      $scope.Posts = result.data.Items;
      $scope.page = result.data.Page;
      $scope.pagesCount = result.data.TotalPages;
      $scope.totalCount = result.data.TotalCount;

      $timeout(function () { $scope.loadingPosts = false }, 500); // display spinner for 1 second
    }

    function loadPostsFailed() {
      notificationService.displayError("Failed to load posts");
    }

    function loadCategories() {
      $scope.loadingCategories = true;

      if ($scope.categoryId) {
       var config = {
         params: {
           page: 0,
           pageSize: 3,
           filter: $scope.filterPosts
         }
       };
       apiService.get('/api/categories/'+ $scope.categoryId, config,
         loadCategoriesCompleted,
         loadCategoriesFailed);
      } else {
        apiService.get('/api/categories/', null,
          loadCategoriesCompleted,
          loadCategoriesFailed);
      }
    }

    function loadCategoriesCompleted(result) {
      $scope.Categories = result.data;
      $scope.loadingCategories = false;
    }

    function loadCategoriesFailed() {
      notificationService.displayError("Failed to load tags");
    }

    function loadTags() {
      apiService.get('/api/tags/', null,
      loadTagsCompleted,
      loadTagsFailed);
    }

    function loadTagsCompleted(result) {
      $scope.Tags = result.data;
      $scope.loadingTags = false;
    }

    function loadTagsFailed() {
      notificationService.displayError("Failed to load tags");
    }

    $scope.search();
    $scope.loadCategories();
    $scope.loadTags();
  }

})(angular.module('HoughtonJoeBlog'));
