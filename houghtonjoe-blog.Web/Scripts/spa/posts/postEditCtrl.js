﻿(function (app) {
    'use strict';

    app.controller('postEditCtrl', postEditCtrl);

    postEditCtrl.$inject = ['$scope', '$rootScope', '$location', '$routeParams', 'apiService', 'notificationService'];

    function postEditCtrl($scope, $rootScope, $location, $routeParams, apiService, notificationService) {
      $scope.pageClass = 'edit-post';
      $scope.post = {};
      $scope.isReadOnly = false;
      $scope.updatePost = updatePost;
      $scope.currentUser = $rootScope.repository.loggedUser;
      $scope.selectedPostId = $routeParams.selectedPostId;
      $scope.redirectToPost = redirectToPost;

      function loadPost() {
        $scope.loadingPost = true;
        apiService.get('/api/posts/detail/' + $scope.selectedPostId, null,
        postLoadCompleted,
        postLoadFailed);
      }

      function postLoadCompleted(result) {
        $scope.post = result.data;
        $scope.loadingPost = false;
      }

      function postLoadFailed() {
        notificationService.displayError("Failed to load post");
        $location.url('/posts/' + $scope.selectedPostId);
      }

      function updatePost() {
        apiService.post('/api/posts/update', $scope.post,
        updatePostSucceded,
        updatePostFailed);
      }

      function updatePostSucceded() {
        notificationService.displaySuccess($scope.post.Title + ' has been updated');
        $scope.redirectToPost();
      }

      function updatePostFailed() {
        notificationService.displayError("Failed to update post");
      }

      function redirectToPost() {
        $location.url('/posts/' + $scope.selectedPostId);
      }

      loadPost();
    }

})(angular.module('HoughtonJoeBlog'));
