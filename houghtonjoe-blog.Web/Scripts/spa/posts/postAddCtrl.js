﻿(function (app) {
    'use strict';

    app.controller('postAddCtrl', postAddCtrl);

    postAddCtrl.$inject = ['$scope', '$rootScope', '$location', '$routeParams', 'apiService', 'notificationService'];

    function postAddCtrl($scope, $rootScope, $location, $routeParams, apiService, notificationService) {
      $scope.pageClass = 'add-post';
      $scope.post = {};
      $scope.isReadOnly = false;
      $scope.addPost = addPost;
      $scope.currentUser = $rootScope.repository.loggedUser;
      $scope.redirectToHome = redirectToHome;
      $scope.selectedCategoryId = selectedCategoryId;
      $scope.selectedTagId = selectedTagId;

        function selectedCategoryId($item) {
          if ($item) {
            $scope.post.CategoryId = $item.originalObject.Id;
            checkFormIsValid();
          }
          else {
            $scope.post.CategoryId = null;
            checkFormIsValid();
          }
        }

        function selectedTagId($item) {
          if ($item) {
            $scope.post.TagId = $item.originalObject.Id;
            checkFormIsValid();
          }
          else {
            $scope.post.TagId = null;
            checkFormIsValid();
          }
        }

        function checkFormIsValid() {
          if ($scope.post.CategoryId && $scope.post.TagId) {
            $scope.isEnabled = true;
          }
          else {
            $scope.isEnabled = false;
          }
        }

        function addPost() {
          apiService.post('/api/posts/add', $scope.post,
            addPostSucceded,
            addPostFailed);
        }

        function addPostSucceded(response) {
          $scope.post = response.data;
           notificationService.displaySuccess('Post created');
           redirectToHome();
        }

        function addPostFailed() {
            notificationService.displayError('Failed to create post');
        }

        function redirectToHome() {
          $location.url('/');
        }
    }

})(angular.module('HoughtonJoeBlog'));
