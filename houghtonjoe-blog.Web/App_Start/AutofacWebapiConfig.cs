﻿namespace houghtonjoe_blog.Web
{
    using System.Configuration;
    using System.Data.Entity;
    using System.Reflection;
    using System.Web.Http;
    using Autofac;
    using Autofac.Integration.WebApi;
    using houghtonjoe_blog.Domain.Category;
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.Error;
    using houghtonjoe_blog.Domain.Infrastructure;
    using houghtonjoe_blog.Domain.Post;
    using houghtonjoe_blog.Domain.Repositories;
    using houghtonjoe_blog.Domain.Role;
    using houghtonjoe_blog.Domain.Tag;
    using houghtonjoe_blog.Domain.User;
    using houghtonjoe_blog.Services;
    using houghtonjoe_blog.Services.Abstract;

    public class AutofacWebapiConfig
    {
        public static IContainer Container;
        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<HoughtonJoeBlogContext>()
                          .As<HoughtonJoeBlogContext>()
                          .WithParameter("connectionString", ConfigurationManager.ConnectionStrings["HoughtonJoeBlog"].ConnectionString)
                          .InstancePerLifetimeScope();

            builder.RegisterType<HoughtonJoeBlogContext>().As<DbContext>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterGeneric(typeof(EntityBaseRepository<>)).As(typeof(IEntityBaseRepository<>)).InstancePerRequest();

            // Repositories
            builder.RegisterType<ErrorRepository>().As<IErrorRepository>().InstancePerRequest();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerRequest();
            builder.RegisterType<UserRoleRepository>().As<IUserRoleRepository>().InstancePerRequest();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>().InstancePerRequest();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>().InstancePerRequest();
            builder.RegisterType<PostRepository>().As<IPostRepository>().InstancePerRequest();
            builder.RegisterType<TagRepository>().As<ITagRepository>().InstancePerRequest();

            // Services
            builder.RegisterType<EncryptionService>().As<IEncryptionService>().InstancePerRequest();
            builder.RegisterType<MembershipService>().As<IMembershipService>().InstancePerRequest();

            Container = builder.Build();

            return Container;
        }
    }
}