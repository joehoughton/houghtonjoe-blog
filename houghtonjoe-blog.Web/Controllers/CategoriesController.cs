﻿namespace houghtonjoe_blog.Web.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using houghtonjoe_blog.Domain.Category;
    using houghtonjoe_blog.Domain.Category.Dto;
    using houghtonjoe_blog.Domain.Error;
    using houghtonjoe_blog.Domain.Infrastructure;
    using houghtonjoe_blog.Web.Infrastructure.Core;

    [RoutePrefix("api/categories")]
    public class CategoriesController : ApiControllerBase
    {
        private readonly ICategoryRepository _categoriesRepository;

        public CategoriesController(ICategoryRepository categoriesRepository, IErrorRepository _errorsRepository, IUnitOfWork _unitOfWork)
        : base(_errorsRepository, _unitOfWork)
        {
            _categoriesRepository = categoriesRepository;
        }

        public HttpResponseMessage Get(HttpRequestMessage request, string filter)
        {
            filter = filter.ToLower().Trim();

            return CreateHttpResponse(request, () =>
            {
                var categories = _categoriesRepository.GetAllByFilter(filter);

                HttpResponseMessage response = request.CreateResponse<IEnumerable<CategoryDto>>(HttpStatusCode.OK, categories);

                return response;
            });
        }

        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var categories = _categoriesRepository.GetAll();

                HttpResponseMessage response = request.CreateResponse<IEnumerable<CategoryDto>>(HttpStatusCode.OK, categories);

                return response;
            });
        }
    }
}
