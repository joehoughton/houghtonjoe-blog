﻿namespace houghtonjoe_blog.Web.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using houghtonjoe_blog.Domain.Error;
    using houghtonjoe_blog.Domain.Infrastructure;
    using houghtonjoe_blog.Domain.Tag;
    using houghtonjoe_blog.Domain.Tag.Dtos;
    using houghtonjoe_blog.Web.Infrastructure.Core;

    [RoutePrefix("api/tags")]
    public class TagsController : ApiControllerBase
    {
        private readonly ITagRepository _tagsRepository;

        public TagsController(ITagRepository tagsRepository, IErrorRepository _errorsRepository, IUnitOfWork _unitOfWork)
        : base(_errorsRepository, _unitOfWork)
        {
            _tagsRepository = tagsRepository;
        }

        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var tags = _tagsRepository.GetAll();

                HttpResponseMessage response = request.CreateResponse<IEnumerable<TagDto>>(HttpStatusCode.OK, tags);

                return response;
            });
        }

        public HttpResponseMessage Get(HttpRequestMessage request, string filter)
        {
            filter = filter.ToLower().Trim();

            return CreateHttpResponse(request, () =>
            {
                var tags = _tagsRepository.GetAllByFilter(filter);

                HttpResponseMessage response = request.CreateResponse<IEnumerable<TagDto>>(HttpStatusCode.OK, tags);

                return response;
            });
        }

    }
}
