﻿namespace houghtonjoe_blog.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using houghtonjoe_blog.Domain.Category;
    using houghtonjoe_blog.Domain.Error;
    using houghtonjoe_blog.Domain.Infrastructure;
    using houghtonjoe_blog.Domain.Post;
    using houghtonjoe_blog.Domain.Post.Dto;
    using houghtonjoe_blog.Web.Infrastructure.Core;

    [RoutePrefix("api/posts")]
    public class PostsController : ApiControllerBase
    {
        private readonly IPostRepository _postRepository;
        private readonly ICategoryRepository _categoryRepository;

        public PostsController(IPostRepository postRepository, ICategoryRepository categoryRepository,
        IErrorRepository _errorsRepository, IUnitOfWork _unitOfWork)
        : base(_errorsRepository, _unitOfWork)
        {
            _postRepository = postRepository;
            _categoryRepository = categoryRepository;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("add")]
        public HttpResponseMessage Add(HttpRequestMessage request, PostDto postDto)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response;

                var category = _categoryRepository.GetSingle(postDto.CategoryId);

                if (category == null)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid Category");
                }
                else
                {
                    _postRepository.Add(postDto);
                    response = request.CreateResponse(HttpStatusCode.Created);
                }

                return response;
            });
        }

        [Route("{page:int=0}/{pageSize=3}/{filter?}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request, () =>
            {
                List<PostDto> posts;
                int totalPosts;

                if (!string.IsNullOrEmpty(filter))
                {
                    posts = _postRepository.GetAllByFilter(filter, currentPage, currentPageSize);
                    totalPosts = _postRepository.Count(filter);
                }
                else
                {
                    posts = _postRepository.GetAll(currentPage, currentPageSize);
                    totalPosts = _postRepository.Count();
                }

                PaginationSet<PostDto> pagedSet = new PaginationSet<PostDto>()
                {
                    Page = currentPage,
                    TotalCount = totalPosts,
                    TotalPages = (int)Math.Ceiling((decimal)totalPosts / currentPageSize),
                    Items = posts
                };

                HttpResponseMessage response = request.CreateResponse<PaginationSet<PostDto>>(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [HttpGet]
        [Route("category/{categoryId:int=0}/{page:int=0}/{pageSize=4}/{filter?}")]
        public HttpResponseMessage GetByCategoryId(HttpRequestMessage request, int categoryId, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request, () =>
            {
                var posts = _postRepository.GetByCategoryId(categoryId, currentPage, currentPageSize);
                var totalPosts = _categoryRepository.CountPosts(categoryId);

                PaginationSet<PostDto> pagedSet = new PaginationSet<PostDto>()
                {
                    Page = currentPage,
                    TotalCount = totalPosts,
                    TotalPages = (int)Math.Ceiling((decimal)totalPosts / currentPageSize),
                    Items = posts
                };

                HttpResponseMessage response = request.CreateResponse<PaginationSet<PostDto>>(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [HttpGet]
        [Route("tag/{tagId:int=0}/{page:int=0}/{pageSize=4}/{filter?}")]
        public HttpResponseMessage GetByTagId(HttpRequestMessage request, int tagId, int? page, int? pageSize, string filter = null)
        {
            int currentPage = page.Value;
            int currentPageSize = pageSize.Value;

            return CreateHttpResponse(request, () =>
            {
                var posts = _postRepository.GetByTagId(tagId, currentPage, currentPageSize);
                var totalPosts = _postRepository.CountPostsWithTag(tagId);

                PaginationSet<PostDto> pagedSet = new PaginationSet<PostDto>()
                {
                    Page = currentPage,
                    TotalCount = totalPosts,
                    TotalPages = (int)Math.Ceiling((decimal)totalPosts / currentPageSize),
                    Items = posts
                };

                HttpResponseMessage response = request.CreateResponse<PaginationSet<PostDto>>(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("detail/{id:int}")]
        public HttpResponseMessage Get(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var postDto = _postRepository.GetSingle(id);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, postDto);

                return response;
            });
        }

        [HttpPost]
        [Route("update")]
        public HttpResponseMessage Update(HttpRequestMessage request, PostDto postDto)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response;

                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                               ModelState.Keys.SelectMany(k => ModelState[k].Errors)
                               .Select(m => m.ErrorMessage).ToArray());
                }
                else
                {
                    _postRepository.Update(postDto);
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [HttpDelete]
        [Route("{id:int}")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response;

                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest,
                               ModelState.Keys.SelectMany(k => ModelState[k].Errors)
                               .Select(m => m.ErrorMessage).ToArray());
                }
                else
                {
                    _postRepository.Delete(id);
                    _unitOfWork.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
    }
}
