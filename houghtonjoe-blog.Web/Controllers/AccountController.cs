﻿namespace houghtonjoe_blog.Web.Controllers
{
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using houghtonjoe_blog.Domain.Error;
    using houghtonjoe_blog.Domain.Infrastructure;
    using houghtonjoe_blog.Domain.User.Models;
    using houghtonjoe_blog.Services.Abstract;
    using houghtonjoe_blog.Services.Utilities;
    using houghtonjoe_blog.Web.Infrastructure.Core;
    using houghtonjoe_blog.Web.Models;

    [Authorize(Roles="Admin")]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiControllerBase
    {
        private readonly IMembershipService _membershipService;

        public AccountController(IMembershipService membershipService, IErrorRepository _errorsRepository, IUnitOfWork _unitOfWork)
        : base(_errorsRepository, _unitOfWork)
        {
            _membershipService = membershipService;
        }

        [AllowAnonymous]
        [Route("authenticate")]
        [HttpPost]
        public HttpResponseMessage Login(HttpRequestMessage request, LoginViewModel user)
        {
            return this.CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response;

                // call fluent validator
                var validateUser = user.Validate(new ValidationContext(user, null, null));

                // if no errors returned by fluent validator, continue with the login process
                if (!validateUser.Any())
                {
                    MembershipContext _userContext = _membershipService.ValidateUser(user.Username, user.Password);

                    if (_userContext.User != null)
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, new { success = true });
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, new { success = false });
                    }
                }
                else // LoginViewModel did not pass fluent validation, return unsuccessful
                    response = request.CreateResponse(HttpStatusCode.OK, new { success = false });

                return response;
            });
        }

        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public HttpResponseMessage Register(HttpRequestMessage request, RegistrationViewModel user)
        {
            return this.CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response;

                // call fluent validator
                var validateUser = user.Validate(new ValidationContext(user, null, null));

                // check the model state is valid, if errors exist, return bad request
                if (validateUser.Any())
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, new { success = false });
                }
                else
                {   // create the user. The membership service returns the user object
                    User _user = _membershipService.CreateUser(user.Username, user.Email, user.Password, new[] { 1 });

                    // successfully created
                    if (_user != null)
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, new { success = true });
                    }
                    else // failed to add user to database
                    {
                        response = request.CreateResponse(HttpStatusCode.OK, new { success = false });
                    }
                }

                return response;
            });
        }

    }
}
