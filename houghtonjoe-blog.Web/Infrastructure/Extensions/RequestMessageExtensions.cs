﻿namespace houghtonjoe_blog.Web.Infrastructure.Extensions
{
    using System.Net.Http;
    using System.Web.Http.Dependencies;
    using houghtonjoe_blog.Services.Abstract;

    public static class RequestMessageExtensions
    {
        internal static IMembershipService GetMembershipService(this HttpRequestMessage request)
        {
            return request.GetService<IMembershipService>();
        }

        private static TService GetService<TService>(this HttpRequestMessage request)
        {
            IDependencyScope dependencyScope = request.GetDependencyScope();
            TService service = (TService)dependencyScope.GetService(typeof(TService));

            return service;
        }
    }
}