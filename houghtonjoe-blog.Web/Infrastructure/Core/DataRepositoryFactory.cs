﻿namespace houghtonjoe_blog.Web.Infrastructure.Core
{
    using System.Net.Http;
    using houghtonjoe_blog.Domain.Generic;
    using houghtonjoe_blog.Domain.Repositories;

    public class DataRepositoryFactory 
    {
    
    }

    public interface IDataRepositoryFactory
    {
        IEntityBaseRepository<T> GetDataRepository<T>(HttpRequestMessage request) where T : class, IEntityBase, new();
    }
}