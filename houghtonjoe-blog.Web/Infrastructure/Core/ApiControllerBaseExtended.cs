﻿namespace houghtonjoe_blog.Web.Infrastructure.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using houghtonjoe_blog.Domain.Error;
    using houghtonjoe_blog.Domain.Error.Models;
    using houghtonjoe_blog.Domain.Infrastructure;

    public class ApiControllerBaseExtended : ApiController
    {
        protected List<Type> _requiredRepositories;
        protected readonly IDataRepositoryFactory _dataRepositoryFactory;
        protected IErrorRepository _errorsRepository;
        protected IUnitOfWork _unitOfWork;

        public ApiControllerBaseExtended(IDataRepositoryFactory dataRepositoryFactory, IUnitOfWork unitOfWork)
        {
            _dataRepositoryFactory = dataRepositoryFactory;
            _unitOfWork = unitOfWork;
        }

        protected HttpResponseMessage CreateHttpResponse(HttpRequestMessage request, List<Type> repos, Func<HttpResponseMessage> function)
        {
            HttpResponseMessage response;

            try
            {
                InitRepositories(repos);
                response = function.Invoke();
            }
            catch (DbUpdateException ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.BadRequest, ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                LogError(ex);
                response = request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }
        
        private void InitRepositories(List<Type> entities)
        {
           // _errorsRepository = _dataRepositoryFactory.GetDataRepository<Error>(requestMessage);
        }

        private void LogError(Exception ex)
        {
            try
            {
                Error _error = new Error {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    DateCreated = DateTime.Now
                };

                _errorsRepository.Add(_error);
                _unitOfWork.Commit();
            }
            catch { }
        }
    }
}