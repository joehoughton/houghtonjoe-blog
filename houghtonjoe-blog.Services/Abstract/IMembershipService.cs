﻿namespace houghtonjoe_blog.Services.Abstract
{
    using System.Collections.Generic;
    using houghtonjoe_blog.Domain.Role.Models;
    using houghtonjoe_blog.Domain.User.Models;
    using houghtonjoe_blog.Services.Utilities;

    public interface IMembershipService
    {
        MembershipContext ValidateUser(string username, string password);
        User CreateUser(string username, string email, string password, int[] roles);
        User GetUser(int userId);
        List<Role> GetUserRoles(string username);
    }
}
