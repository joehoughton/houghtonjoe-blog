﻿namespace houghtonjoe_blog.Services.Utilities
{
    using System.Security.Principal;
    using houghtonjoe_blog.Domain.User.Models;

    public class MembershipContext
    {
        public IPrincipal Principal { get; set; }
        public User User { get; set; }
        public bool IsValid()
        {
            return Principal != null;
        }
    }
}
