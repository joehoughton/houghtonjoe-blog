namespace houghtonjoe_blog.Domain.PostTag.Models
{
    using houghtonjoe_blog.Domain.Post.Models;
    using houghtonjoe_blog.Domain.Tag.Models;

    public class PostTag
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int TagId { get; set; }
        public virtual Post Post { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
