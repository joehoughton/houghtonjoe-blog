namespace houghtonjoe_blog.Domain.PostTag.Mapping
{
    using System.Data.Entity.ModelConfiguration;
    using houghtonjoe_blog.Domain.PostTag.Models;

    public class PostTagMap : EntityTypeConfiguration<PostTag>
    {
        public PostTagMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            ToTable("PostTag");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.PostId).HasColumnName("PostId");
            Property(t => t.TagId).HasColumnName("TagId");

            // Relationships
            HasRequired(t => t.Post)
                .WithMany()
                .HasForeignKey(d => d.PostId);
            HasRequired(t => t.Tag)
                .WithMany(t => t.PostTags)
                .HasForeignKey(d => d.TagId);
        }
    }
}
