﻿namespace houghtonjoe_blog.Domain.Generic
{
    public interface IEntityBase
    {
        int Id { get; set; }
    }
}
