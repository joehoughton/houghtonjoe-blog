﻿namespace houghtonjoe_blog.Domain.Role
{
    using System.Linq;
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.Role.Models;

    public class RoleRepository : IRoleRepository
    {
        private readonly HoughtonJoeBlogContext _context;

        public RoleRepository(HoughtonJoeBlogContext context)
        {
            _context = context;
        }

        public Role GetSingle(int roleId)
        {
            return _context.Roles.FirstOrDefault(x => x.Id == roleId);
        }
    }
}
