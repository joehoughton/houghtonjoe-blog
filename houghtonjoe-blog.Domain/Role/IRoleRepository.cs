﻿namespace houghtonjoe_blog.Domain.Role
{
    using houghtonjoe_blog.Domain.Role.Models;

    public interface IRoleRepository
    {
        Role GetSingle(int roleId);
    }
}
