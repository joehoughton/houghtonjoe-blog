namespace houghtonjoe_blog.Domain.Role.Models
{
    using System.Collections.Generic;

    using houghtonjoe_blog.Domain.User.Models;

    public class Role
    {
        public Role()
        {
            UserRoles = new List<UserRole>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
