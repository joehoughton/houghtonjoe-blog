﻿namespace houghtonjoe_blog.Domain.Error
{
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.Error.Models;

    public class ErrorRepository : IErrorRepository
    {
        private readonly HoughtonJoeBlogContext _context;

        public ErrorRepository(HoughtonJoeBlogContext context)
         {
            _context = context;
         }

        public void Add(Error error)
        {
            _context.Errors.Add(error);
        }
    }
}
