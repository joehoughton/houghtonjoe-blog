namespace houghtonjoe_blog.Domain.Error.Models
{
    using System;

    public class Error
    {
        public int ID { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
