﻿namespace houghtonjoe_blog.Domain.Error
{
    using houghtonjoe_blog.Domain.Error.Models;

    public interface IErrorRepository
    {
        void Add(Error error);
    }
}
