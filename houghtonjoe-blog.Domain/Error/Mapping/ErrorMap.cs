namespace houghtonjoe_blog.Domain.Error.Mapping
{
    using System.Data.Entity.ModelConfiguration;
    using houghtonjoe_blog.Domain.Error.Models;

    public class ErrorMap : EntityTypeConfiguration<Error>
    {
        public ErrorMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            ToTable("Error");
            Property(t => t.ID).HasColumnName("ID");
            Property(t => t.Message).HasColumnName("Message");
            Property(t => t.StackTrace).HasColumnName("StackTrace");
            Property(t => t.DateCreated).HasColumnName("DateCreated");
        }
    }
}
