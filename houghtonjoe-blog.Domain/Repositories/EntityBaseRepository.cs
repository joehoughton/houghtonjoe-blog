﻿namespace houghtonjoe_blog.Domain.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Linq.Expressions;
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.Generic;
    using houghtonjoe_blog.Domain.Infrastructure;

    public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T : class, IEntityBase, new()
    {
        private HoughtonJoeBlogContext dataContext;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected HoughtonJoeBlogContext DbContext
        {
            get { return this.dataContext ?? (this.dataContext = this.DbFactory.Init()); }
        }
        public EntityBaseRepository(IDbFactory dbFactory)
        {
            this.DbFactory = dbFactory;
        }

        public virtual IQueryable<T> GetAll()
        {
            return this.DbContext.Set<T>();
        }
        public virtual IQueryable<T> All
        {
            get
            {
                return this.GetAll();
            }
        }
        public virtual IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = this.DbContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }
        public T GetSingle(int id)
        {
            return this.GetAll().FirstOrDefault(x => x.Id == id);
        }
        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return this.DbContext.Set<T>().Where(predicate);
        }

        public virtual void Add(T entity)
        {
            DbEntityEntry dbEntityEntry = this.DbContext.Entry(entity);
            this.DbContext.Set<T>().Add(entity); 
        }
        public virtual void Edit(T entity)
        {
            DbEntityEntry dbEntityEntry = this.DbContext.Entry(entity);
            dbEntityEntry.State = EntityState.Modified;
        }
        public virtual void Delete(T entity)
        {
            DbEntityEntry dbEntityEntry = this.DbContext.Entry(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }
    }
}
