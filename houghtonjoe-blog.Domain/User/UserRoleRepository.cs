﻿namespace houghtonjoe_blog.Domain.User
{
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.User.Models;

    public class UserRoleRepository : IUserRoleRepository
    {
        private readonly HoughtonJoeBlogContext _context;

        public UserRoleRepository(HoughtonJoeBlogContext context)
        {
            _context = context;
        }

        public void Add(UserRole userRole)
        {
            _context.UserRoles.Add(userRole);
        }
    }
}
