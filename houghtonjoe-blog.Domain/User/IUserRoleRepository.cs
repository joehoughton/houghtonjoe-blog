﻿namespace houghtonjoe_blog.Domain.User
{
    using houghtonjoe_blog.Domain.User.Models;

    public interface IUserRoleRepository
    {
        void Add(UserRole userRole);
    }
}
