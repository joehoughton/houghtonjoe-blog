﻿namespace houghtonjoe_blog.Domain.User
{
    using houghtonjoe_blog.Domain.User.Models;

    public interface IUserRepository
    {
        User Get(string username);
        void Add(User user);
        User GetSingle(int userId);
    }
}
