﻿namespace houghtonjoe_blog.Domain.User
{
    using System.Linq;
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.User.Models;

    public class UserRepository : IUserRepository
    {
        private readonly HoughtonJoeBlogContext _context;

        public UserRepository(HoughtonJoeBlogContext context)
        {
            _context = context;
        }

        public User Get(string username)
        {
            return _context.Users.FirstOrDefault(x => x.Username == username);
        }

        public void Add(User user)
        {
            _context.Users.Add(user);
        }

        public User GetSingle(int userId)
        {
            return _context.Users.FirstOrDefault(x => x.Id == userId);
        }
    }
}
