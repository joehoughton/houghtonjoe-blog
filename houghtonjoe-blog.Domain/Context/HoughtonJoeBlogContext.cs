namespace houghtonjoe_blog.Domain.Context
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using houghtonjoe_blog.Domain.Category.Mappings;
    using houghtonjoe_blog.Domain.Category.Models;
    using houghtonjoe_blog.Domain.Error.Mapping;
    using houghtonjoe_blog.Domain.Error.Models;
    using houghtonjoe_blog.Domain.Post.Mapping;
    using houghtonjoe_blog.Domain.Post.Models;
    using houghtonjoe_blog.Domain.PostTag.Mapping;
    using houghtonjoe_blog.Domain.PostTag.Models;
    using houghtonjoe_blog.Domain.Role.Mapping;
    using houghtonjoe_blog.Domain.Role.Models;
    using houghtonjoe_blog.Domain.Tag.Mapping;
    using houghtonjoe_blog.Domain.Tag.Models;
    using houghtonjoe_blog.Domain.User.Mapping;
    using houghtonjoe_blog.Domain.User.Models;

    public class HoughtonJoeBlogContext : DbContext
    {
        static HoughtonJoeBlogContext()
        {
            Database.SetInitializer<HoughtonJoeBlogContext>(null);
        }

        public HoughtonJoeBlogContext(string connectionString)
            : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public virtual void Commit()
        {
            this.SaveChanges();
        }

        public DbSet<Error> Errors { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostTag> PostTags { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new ErrorMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new CategoryMap());
            modelBuilder.Configurations.Add(new PostMap());
            modelBuilder.Configurations.Add(new PostTagMap());
            modelBuilder.Configurations.Add(new TagMap());
        }
    }
}
