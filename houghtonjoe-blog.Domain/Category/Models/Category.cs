namespace houghtonjoe_blog.Domain.Category.Models
{
    using System.Collections.Generic;
    using houghtonjoe_blog.Domain.Post.Models;
    using System;

    public class Category
    {
        public Category()
        {
            Posts = new List<Post>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}
