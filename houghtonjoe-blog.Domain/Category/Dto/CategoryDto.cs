﻿namespace houghtonjoe_blog.Domain.Category.Dto
{
    using System;

    public class CategoryDto
    {
        public CategoryDto() { }
        public CategoryDto(int id, string name, DateTime dateCreated, int categoryCount)
        {
            Id = id;
            Name = name;
            DateCreated = dateCreated;
            Count = categoryCount;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public int Count { get; set; }
    }
}
