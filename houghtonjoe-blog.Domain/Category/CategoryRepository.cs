﻿namespace houghtonjoe_blog.Domain.Category
{
    using System.Collections.Generic;
    using System.Linq;
    using houghtonjoe_blog.Domain.Category.Dto;
    using houghtonjoe_blog.Domain.Context;

    public class CategoryRepository : ICategoryRepository
    {
        private readonly HoughtonJoeBlogContext _context;

        public CategoryRepository(HoughtonJoeBlogContext context)
        {
            _context = context;
        }

        public List<CategoryDto> GetAll()
        {
            var categories = _context.Categories
                             .OrderBy(c => c.Name)
                             .ThenByDescending(c => c.DateCreated)
                             .Select(c => new CategoryDto() { Id = c.Id, Name = c.Name, DateCreated = c.DateCreated, Count = _context.Posts.Count(p => p.CategoryId == c.Id)});

            return categories.ToList();
        }

        public int CountPosts(int categoryId)
        {
            var count = _context.Posts.Count(c => c.CategoryId == categoryId);
            return count;
        }

        public List<CategoryDto> GetAllByFilter(string filter)
        {
            var categories = _context.Categories
                             .Where(c => c.Name.ToLower().Contains(filter))
                             .OrderBy(c => c.Name)
                             .ThenByDescending(c => c.DateCreated)
                             .Select(c => new CategoryDto() { Id = c.Id, Name = c.Name, DateCreated = c.DateCreated });

            return categories.ToList();
        }

        public CategoryDto GetSingle(int id)
        {
            var category = _context.Categories.Single(x => x.Id == id);

            var categoryDto = new CategoryDto()
            {
                Id = category.Id, Name = category.Name, DateCreated = category.DateCreated
            };

            return categoryDto;
        }

    }
}
