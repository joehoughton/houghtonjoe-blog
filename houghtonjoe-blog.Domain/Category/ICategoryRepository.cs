﻿namespace houghtonjoe_blog.Domain.Category
{
    using System.Collections.Generic;
    using houghtonjoe_blog.Domain.Category.Dto;

    public interface ICategoryRepository
    {
        List<CategoryDto> GetAllByFilter(string filter);
        CategoryDto GetSingle(int id);
        List<CategoryDto> GetAll();
        int CountPosts(int categoryId);
    }
}
