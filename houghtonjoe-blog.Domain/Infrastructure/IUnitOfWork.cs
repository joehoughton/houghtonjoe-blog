﻿namespace houghtonjoe_blog.Domain.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
