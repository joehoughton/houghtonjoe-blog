﻿namespace houghtonjoe_blog.Domain.Infrastructure
{
    using houghtonjoe_blog.Domain.Context;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory dbFactory;
        private HoughtonJoeBlogContext dbContext;

        public UnitOfWork(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        public HoughtonJoeBlogContext DbContext
        {
            get { return this.dbContext ?? (this.dbContext = this.dbFactory.Init()); }
        }

        public void Commit()
        {
            this.DbContext.Commit();
        }
    }
}
