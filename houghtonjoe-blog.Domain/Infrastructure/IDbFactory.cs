﻿namespace houghtonjoe_blog.Domain.Infrastructure
{
    using System;
    using houghtonjoe_blog.Domain.Context;

    public interface IDbFactory : IDisposable
    {
        HoughtonJoeBlogContext Init();
    }
}
