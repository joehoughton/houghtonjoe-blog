﻿namespace houghtonjoe_blog.Domain.Infrastructure
{
    using houghtonjoe_blog.Domain.Context;

    public class DbFactory : Disposable, IDbFactory
    {
        private readonly HoughtonJoeBlogContext _context;
        public DbFactory(HoughtonJoeBlogContext context)
        {
            _context = context;
        }

        public HoughtonJoeBlogContext Init()
        {
        return _context;
        }

        protected override void DisposeCore()
        {
            if (_context != null){
                _context.Dispose();
            }
        }
    }
}
