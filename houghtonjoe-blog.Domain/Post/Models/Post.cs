namespace houghtonjoe_blog.Domain.Post.Models
{
    using houghtonjoe_blog.Domain.Category.Models;
    using System;

    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }
        public string SourceCode { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
