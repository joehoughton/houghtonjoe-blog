﻿namespace houghtonjoe_blog.Domain.Post.Dto
{
    using System;
    using System.Collections.Generic;
    using houghtonjoe_blog.Domain.Tag.Dtos;

    public class PostDto
    {
        public PostDto() { }
        public PostDto(int id, string title, DateTime datecreated, string description, string sourceCode, int categoryId, List<TagDto> tags, int tagId)
        {
            Id = id;
            Title = title;
            DateCreated = datecreated;
            Description = description;
            SourceCode = sourceCode;
            CategoryId = categoryId;
            Tags = tags;
            TagId = tagId;
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }
        public string SourceCode { get; set; }
        public int CategoryId { get; set; }
        public List<TagDto> Tags { get; set; }
        public int TagId { get; set; }
    }
}
