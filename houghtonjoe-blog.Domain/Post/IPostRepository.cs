﻿namespace houghtonjoe_blog.Domain.Post
{
    using System.Collections.Generic;
    using houghtonjoe_blog.Domain.Post.Dto;

    public interface IPostRepository
    {
        PostDto Add(PostDto postDto);
        List<PostDto> GetAll(int currentPage, int currentPageSize);
        List<PostDto> GetAllByFilter(string filter, int currentPage, int currentPageSize);
        List<PostDto> GetByCategoryId(int categoryId, int currentPage, int currentPageSize);
        List<PostDto> GetByTagId(int tagId, int currentPage, int currentPageSize);
        PostDto GetSingle(int postId);
        int Count();
        int Count(string filter);
        int CountPostsWithTag(int tagId);
        void Update(PostDto postDto);
        void Delete(int postId);
    }
}