namespace houghtonjoe_blog.Domain.Post.Mapping
{
    using System.Data.Entity.ModelConfiguration;
    using houghtonjoe_blog.Domain.Post.Models;

    public class PostMap : EntityTypeConfiguration<Post>
    {
        public PostMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.Description)
                .IsRequired();

            Property(t => t.SourceCode);

            // Table & Column Mappings
            ToTable("Post");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Title).HasColumnName("Title");
            Property(t => t.DateCreated).HasColumnName("DateCreated");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.CategoryId).HasColumnName("CategoryId");

            // Relationships
            HasRequired(t => t.Category)
                .WithMany(t => t.Posts)
                .HasForeignKey(d => d.CategoryId);
        }
    }
}
