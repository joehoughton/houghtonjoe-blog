﻿namespace houghtonjoe_blog.Domain.Post
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.Post.Dto;
    using houghtonjoe_blog.Domain.Post.Models;
    using houghtonjoe_blog.Domain.PostTag.Models;
    using houghtonjoe_blog.Domain.Tag.Dtos;

    public class PostRepository : IPostRepository
    {
        private readonly HoughtonJoeBlogContext _context;

        public PostRepository(HoughtonJoeBlogContext context)
        {
            _context = context;
        }

        public PostDto Add(PostDto postDto)
        {
            var post = new Post()
            {
                DateCreated = DateTime.Now,
                Description = postDto.Description,
                Title = postDto.Title,
                SourceCode = postDto.SourceCode,
                CategoryId = postDto.CategoryId,
            };

            _context.Posts.Add(post);
            _context.SaveChanges();

            var postTag = new PostTag() {
                PostId = post.Id,
                TagId = postDto.TagId
            };

            _context.PostTags.Add(postTag);
            _context.SaveChanges();

            var newPostDto = new PostDto()
            {
                Id = post.Id,
                DateCreated = post.DateCreated,
                Description = post.Description,
                Title = post.Title,
                SourceCode = post.SourceCode,
                CategoryId = post.CategoryId
            };

            return newPostDto;
        }

        public List<PostDto> GetAll(int currentPage, int currentPageSize)
        {
            var posts = _context.Posts
                        .OrderBy(p => p.Title)
                        .ThenByDescending(p => p.DateCreated)
                        .Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .Select(p => new PostDto()
                        {
                            Id = p.Id,
                            Description = p.Description,
                            Title = p.Title,
                            SourceCode = p.SourceCode,
                            CategoryId = p.CategoryId,
                            DateCreated = p.DateCreated
                       }).ToList();

            foreach (var post in posts)
            {
                var tags = _context.PostTags.Where(pt => pt.PostId == post.Id).ToList();
                var tagList = tags.Select(tag => new TagDto() { Id = tag.Tag.Id, DateCreated = tag.Tag.DateCreated, Title = tag.Tag.Title }).ToList();
                post.Tags = tagList;
            }

            return posts;
        }

        public List<PostDto> GetAllByFilter(string filter, int currentPage, int currentPageSize)
        {
            var posts = _context.Posts
                        .Where(p => p.Title.ToLower().Contains(filter.ToLower().Trim()))
                        .OrderBy(p => p.Title)
                        .ThenByDescending(p => p.DateCreated)
                        .Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .Select(p => new PostDto()
                        {
                            Id = p.Id,
                            Description = p.Description,
                            Title = p.Title,
                            SourceCode = p.SourceCode,
                            CategoryId = p.CategoryId,
                            DateCreated = p.DateCreated
                        })
                        .ToList();
            return posts;
        }

        public List<PostDto> GetByCategoryId(int categoryId, int currentPage, int currentPageSize)
        {
            var posts = _context.Posts
                        .Where(p => p.CategoryId == categoryId)
                        .OrderBy(p => p.Title)
                        .ThenByDescending(p => p.DateCreated)
                        .Skip(currentPage * currentPageSize)
                        .Take(currentPageSize)
                        .Select(p => new PostDto()
                        {
                            Id = p.Id,
                            Description = p.Description,
                            Title = p.Title,
                            SourceCode = p.SourceCode,
                            CategoryId = p.CategoryId,
                            DateCreated = p.DateCreated
                        })
                        .ToList();

            foreach (var post in posts)
            {
                var tags = _context.PostTags.Where(pt => pt.PostId == post.Id).ToList();
                var tagList = tags.Select(tag => new TagDto() { Id = tag.Tag.Id, DateCreated = tag.Tag.DateCreated, Title = tag.Tag.Title }).ToList();
                post.Tags = tagList;
            }
            return posts;
        }

        public List<PostDto> GetByTagId(int tagId, int currentPage, int currentPageSize)
        {
            var posts = _context.PostTags
                    .Where(p => p.TagId == tagId)
                    .OrderBy(p => p.Post.Title)
                    .ThenByDescending(p => p.Post.DateCreated)
                    .Skip(currentPage * currentPageSize)
                    .Take(currentPageSize)
                    .Select(p => new PostDto()
                    {
                        Id = p.Post.Id,
                        Description = p.Post.Description,
                        Title = p.Post.Title,
                        SourceCode = p.Post.SourceCode,
                        CategoryId = p.Post.CategoryId,
                        DateCreated = p.Post.DateCreated
                    })
                    .ToList();

            foreach (var post in posts)
            {
                var tags = _context.PostTags.Where(pt => pt.PostId == post.Id).ToList();
                var tagList = tags.Select(tag => new TagDto() { Id = tag.Tag.Id, DateCreated = tag.Tag.DateCreated, Title = tag.Tag.Title }).ToList();
                post.Tags = tagList;
            }

            return posts;
        }

        public PostDto GetSingle(int postId)
        {
            var post = _context.Posts.Single(p => p.Id == postId);

            var postDto = new PostDto()
            {
                Id = post.Id,
                Description = post.Description,
                Title = post.Title,
                SourceCode = post.SourceCode,
                CategoryId = post.CategoryId,
                DateCreated = post.DateCreated
            };

            var tags = _context.PostTags.Where(pt => pt.PostId == post.Id).ToList();
            var tagList = tags.Select(tag => new TagDto() { Id = tag.Tag.Id, DateCreated = tag.Tag.DateCreated, Title = tag.Tag.Title }).ToList();
            postDto.Tags = tagList;

            return postDto;
        }

        public int Count()
        {
            var postCount = _context.Posts.Count();
            return postCount;
        }

        public int Count(string filter)
        {
            var postCount = _context.Posts
                            .Count(b => b.Title.ToLower()
                            .Contains(filter.ToLower().Trim()));

            return postCount;
        }

        public int CountPostsWithTag(int tagId)
        {
            var postCount = _context.PostTags.Count(p => p.Tag.Id == tagId);
            return postCount;
        }

        public void Update(PostDto postDto)
        {
            var post = _context.Posts.Single(p => p.Id == postDto.Id);
            post.Title = postDto.Title;
            post.Description = postDto.Description;
            post.SourceCode = postDto.SourceCode;

            _context.SaveChanges();
        }

        public void Delete(int postId)
        {
            var post = _context.Posts.Single(p => p.Id == postId);
            var postTag= _context.PostTags.Where(p => p.PostId == postId).ToList();

            if (postTag.Any()) {
                foreach (var x in postTag)
                {
                    _context.PostTags.Remove(x);
                }
            }

            _context.Posts.Remove(post);
            _context.SaveChanges();
        }
    }
}
