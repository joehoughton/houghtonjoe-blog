namespace houghtonjoe_blog.Domain.Tag.Mapping
{
    using System.Data.Entity.ModelConfiguration;
    using houghtonjoe_blog.Domain.Tag.Models;

    public class TagMap : EntityTypeConfiguration<Tag>
    {
        public TagMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("Tag");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Title).HasColumnName("Title");
            Property(t => t.DateCreated).HasColumnName("DateCreated");
        }
    }
}
