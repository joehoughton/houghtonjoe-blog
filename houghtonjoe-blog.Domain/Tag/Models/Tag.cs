namespace houghtonjoe_blog.Domain.Tag.Models
{
    using System;
    using System.Collections.Generic;
    using houghtonjoe_blog.Domain.PostTag.Models;

    public class Tag
    {
        public Tag()
        {
            PostTags = new List<PostTag>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}
