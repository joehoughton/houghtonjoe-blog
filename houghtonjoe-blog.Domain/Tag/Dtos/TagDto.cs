﻿namespace houghtonjoe_blog.Domain.Tag.Dtos
{
    using System;

    public class TagDto
    {
        public TagDto() { }
        public TagDto(int id, string title, DateTime dateCreated)
        {
            Id = id;
            Title = title;
            DateCreated = dateCreated;
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
