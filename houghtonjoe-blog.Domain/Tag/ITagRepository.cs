﻿namespace houghtonjoe_blog.Domain.Tag
{
    using System.Collections.Generic;
    using houghtonjoe_blog.Domain.Tag.Dtos;

    public interface ITagRepository
    {
        List<TagDto> GetAll();
        List<TagDto> GetAllByFilter(string filter);
    }
}
