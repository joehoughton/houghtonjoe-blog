﻿namespace houghtonjoe_blog.Domain.Tag
{
    using System.Collections.Generic;
    using System.Linq;
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.Tag.Dtos;

    public class TagRepository : ITagRepository
    {
       private readonly HoughtonJoeBlogContext _context;

       public TagRepository(HoughtonJoeBlogContext context)
        {
            _context = context;
        }

        public List<TagDto> GetAll()
        {
            var tags = _context.Tags
                        .OrderBy(t=>t.Title)
                        .ThenBy(t => t.DateCreated)
                        .Select(t => new TagDto()
                        {
                            Id = t.Id,
                            Title = t.Title,
                            DateCreated = t.DateCreated
                        })
                        .ToList();

            return tags;
        }

        public List<TagDto> GetAllByFilter(string filter)
        {
            var tags = _context.Tags
                             .Where(t => t.Title.ToLower().Contains(filter))
                             .OrderBy(t => t.Title)
                             .ThenByDescending(t => t.DateCreated)
                             .Select(t => new TagDto() { Id = t.Id, Title = t.Title, DateCreated = t.DateCreated });

            return tags.ToList();
        }
    }
}
