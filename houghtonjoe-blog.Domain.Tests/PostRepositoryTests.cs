﻿namespace houghtonjoe_blog.Domain.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Linq;
    using houghtonjoe_blog.Domain.Context;
    using houghtonjoe_blog.Domain.Post;
    using houghtonjoe_blog.Domain.Post.Dto;
    using houghtonjoe_blog.Domain.Post.Models;
    using houghtonjoe_blog.Domain.PostTag.Models;
    using houghtonjoe_blog.Domain.Tag.Dtos;
    using houghtonjoe_blog.Domain.Tag.Models;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    class PostRepositoryTests
    {
        private HoughtonJoeBlogContext context;
        private readonly PostRepository _postRepository;
        private readonly List<Post> _postList;
        private readonly List<PostTag> _postTagList;
        private readonly List<Tag> _tagList;

        /// <summary>
        /// Constructor creates a mock context class with a mocked DbSet, which is passed into the PostRepository.
        /// </summary>
        public PostRepositoryTests()
        {
            // create connection string so we're not reliant on Web.config connection strings
             var connectionString = new SqlConnectionStringBuilder() { DataSource = ".", IntegratedSecurity = true, InitialCatalog = "RandomDbName" }.ConnectionString;

            // pass connection string to context class
            context = new HoughtonJoeBlogContext(connectionString);

            _postList = new List<Post>
            {
                new Post(){ Id = 1, Title = "Post Title B", Description = "Post Description B", DateCreated = new DateTime(2016,1,10,18,30,00), SourceCode = "Source Code", CategoryId = 1},
                new Post(){ Id = 2, Title = "Post Title D", Description = "Post Description D", DateCreated = new DateTime(2016,1,10,18,31,00), SourceCode = "Source Code", CategoryId = 1},
                new Post(){ Id = 3, Title = "Post Title H", Description = "Post Description H", DateCreated = new DateTime(2016,1,10,18,32,00), SourceCode = "Source Code", CategoryId = 1},
                new Post(){ Id = 4, Title = "Post Title G", Description = "Post Description G", DateCreated = new DateTime(2016,1,10,18,33,00), SourceCode = "Source Code", CategoryId = 1},
                new Post(){ Id = 5, Title = "Post Title E", Description = "Post Description E", DateCreated = new DateTime(2016,1,10,18,34,00), SourceCode = "Source Code", CategoryId = 1},
                new Post(){ Id = 6, Title = "Post Title F", Description = "Post Description F", DateCreated = new DateTime(2016,1,10,18,35,00), SourceCode = "Source Code", CategoryId = 1},
                new Post(){ Id = 7, Title = "Post Title I", Description = "Post Description I", DateCreated = new DateTime(2016,1,10,18,36,00), SourceCode = "Source Code", CategoryId = 1},
                new Post(){ Id = 8, Title = "Post Title L", Description = "Post Description L", DateCreated = new DateTime(2016,1,10,18,37,00), SourceCode = "Source Code", CategoryId = 1},
                new Post(){ Id = 9, Title = "Post Title A", Description = "Post Description A", DateCreated = new DateTime(2016,1,10,18,38,00), SourceCode = "Source Code", CategoryId = 1}
            };

            _postTagList = new List<PostTag>()
            {
                new PostTag(){Id = 1, PostId = 1, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}},
                new PostTag(){Id = 2, PostId = 2, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}},
                new PostTag(){Id = 3, PostId = 3, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}},
                new PostTag(){Id = 4, PostId = 4, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}},
                new PostTag(){Id = 5, PostId = 5, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}},
                new PostTag(){Id = 6, PostId = 6, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}},
                new PostTag(){Id = 7, PostId = 7, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}},
                new PostTag(){Id = 8, PostId = 8, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}},
                new PostTag(){Id = 9, PostId = 9, TagId = 1, Tag = new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}}
            };

            _tagList = new List<Tag>()
            {
                new Tag(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}
            };

            // convert IEnumerable lists to IQueryable lists
            IQueryable<Post> queryableList = _postList.AsQueryable();
            IQueryable<PostTag> queryablePostTagList = _postTagList.AsQueryable();
            IQueryable<Tag> queryableTagList = _tagList.AsQueryable();

            // force DbSets to return the IQueryable members of converted list object as its data source
            var mockSetPost = new Mock<DbSet<Post>>();
            mockSetPost.As<IQueryable<Post>>().Setup(m => m.Provider).Returns(queryableList.Provider);
            mockSetPost.As<IQueryable<Post>>().Setup(m => m.Expression).Returns(queryableList.Expression);
            mockSetPost.As<IQueryable<Post>>().Setup(m => m.ElementType).Returns(queryableList.ElementType);
            mockSetPost.As<IQueryable<Post>>().Setup(m => m.GetEnumerator()).Returns(queryableList.GetEnumerator());

            var mockSetPostTag = new Mock<DbSet<PostTag>>();
            mockSetPostTag.As<IQueryable<PostTag>>().Setup(m => m.Provider).Returns(queryablePostTagList.Provider);
            mockSetPostTag.As<IQueryable<PostTag>>().Setup(m => m.Expression).Returns(queryablePostTagList.Expression);
            mockSetPostTag.As<IQueryable<PostTag>>().Setup(m => m.ElementType).Returns(queryablePostTagList.ElementType);
            mockSetPostTag.As<IQueryable<PostTag>>().Setup(m => m.GetEnumerator()).Returns(queryablePostTagList.GetEnumerator());

            var mockSetTag = new Mock<DbSet<Tag>>();
            mockSetTag.As<IQueryable<Tag>>().Setup(m => m.Provider).Returns(queryableTagList.Provider);
            mockSetTag.As<IQueryable<Tag>>().Setup(m => m.Expression).Returns(queryableTagList.Expression);
            mockSetTag.As<IQueryable<Tag>>().Setup(m => m.ElementType).Returns(queryableTagList.ElementType);
            mockSetTag.As<IQueryable<Tag>>().Setup(m => m.GetEnumerator()).Returns(queryableTagList.GetEnumerator());

            // context class will return mocked DbSet
            context.Posts = mockSetPost.Object;
            context.PostTags = mockSetPostTag.Object;
            context.Tags = mockSetTag.Object;

            // pass context to repository
            _postRepository = new PostRepository(context);
        }

        /// <summary>
        /// GetAll method should return correct number of results.
        /// </summary>
        [Test]
        public void GetAllMethodShouldReturnCorrectNumberOfResults()
        {
            var postDtos = _postRepository.GetAll(0, 6);

            Assert.AreEqual(6, postDtos.Count);
        }

        /// <summary>
        /// GetAll method should return results in correct order, ordered by Title, then by date descending.
        /// </summary>
        [Test]
        public void GetAllMethodShouldReturnInCorrectOrder()
        {
            // predicted list, ordered by title then by date descending
            var expectedDtos = new List<PostDto>
            {
                new PostDto(){ Id = 9, Title = "Post Title A", Description = "Post Description A", DateCreated = new DateTime(2016,1,10,18,38,00), SourceCode = "Source Code", CategoryId = 1, Tags = new List<TagDto>(){new TagDto(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}}},
                new PostDto(){ Id = 1, Title = "Post Title B", Description = "Post Description B", DateCreated = new DateTime(2016,1,10,18,30,00), SourceCode = "Source Code", CategoryId = 1, Tags = new List<TagDto>(){new TagDto(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}}},
                new PostDto(){ Id = 2, Title = "Post Title D", Description = "Post Description D", DateCreated = new DateTime(2016,1,10,18,31,00), SourceCode = "Source Code", CategoryId = 1, Tags = new List<TagDto>(){new TagDto(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}}},
                new PostDto(){ Id = 5, Title = "Post Title E", Description = "Post Description E", DateCreated = new DateTime(2016,1,10,18,34,00), SourceCode = "Source Code", CategoryId = 1, Tags = new List<TagDto>(){new TagDto(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}}},
                new PostDto(){ Id = 6, Title = "Post Title F", Description = "Post Description F", DateCreated = new DateTime(2016,1,10,18,35,00), SourceCode = "Source Code", CategoryId = 1, Tags = new List<TagDto>(){new TagDto(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}}},
                new PostDto(){ Id = 4, Title = "Post Title G", Description = "Post Description G", DateCreated = new DateTime(2016,1,10,18,33,00), SourceCode = "Source Code", CategoryId = 1, Tags = new List<TagDto>(){new TagDto(){Id = 1, Title = "JavaScript", DateCreated = new DateTime(2015,1,03,18,30,00)}}}
            };

            // actual list returned from PostRepository
            var postDtos = _postRepository.GetAll(0, 6);

            // assert first item in list returned
            Assert.AreEqual(expectedDtos[0].Description, postDtos[0].Description);
            Assert.AreEqual(expectedDtos[0].Title, postDtos[0].Title);
            Assert.AreEqual(expectedDtos[0].Id, postDtos[0].Id);
            Assert.AreEqual(expectedDtos[0].SourceCode, postDtos[0].SourceCode);
            Assert.AreEqual(expectedDtos[0].CategoryId, postDtos[0].CategoryId);
            Assert.AreEqual(expectedDtos[0].Tags[0].Id, postDtos[0].Tags[0].Id);
            Assert.AreEqual(expectedDtos[0].Tags[0].Title, postDtos[0].Tags[0].Title);
            Assert.AreEqual(expectedDtos[0].Tags[0].DateCreated, postDtos[0].Tags[0].DateCreated);

            // assert last item in list returned
            Assert.AreEqual(expectedDtos[5].Description, postDtos[5].Description);
            Assert.AreEqual(expectedDtos[5].Title, postDtos[5].Title);
            Assert.AreEqual(expectedDtos[5].Id, postDtos[5].Id);
            Assert.AreEqual(expectedDtos[5].SourceCode, postDtos[5].SourceCode);
            Assert.AreEqual(expectedDtos[5].CategoryId, postDtos[5].CategoryId);
            Assert.AreEqual(expectedDtos[5].Tags[0].Id, postDtos[5].Tags[0].Id);
            Assert.AreEqual(expectedDtos[5].Tags[0].Title, postDtos[5].Tags[0].Title);
            Assert.AreEqual(expectedDtos[5].Tags[0].DateCreated, postDtos[5].Tags[0].DateCreated);
        }

        /// <summary>
        /// GetSingle method should return the correct result
        /// </summary>
        [Test]
        public void GetSingleMethodReturnsCorrectResult()
        {
            // get post by id
            var postDto = _postRepository.GetSingle(9);
            Assert.AreEqual(9, postDto.Id);
            Assert.AreEqual("Post Title A", postDto.Title);
            Assert.AreEqual("Post Description A", postDto.Description);
        }

    }
  
}